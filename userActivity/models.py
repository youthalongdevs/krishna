from django.db import models
from django.conf import settings
from django.utils import timezone

# Create your models here.
class Users(models.Model):
    userName = models.CharField(max_length=256)
    datetime = models.DateTimeField(('datetime'), default=timezone.now)


    class Meta(object):
        default_related_name= "activeUsers"
