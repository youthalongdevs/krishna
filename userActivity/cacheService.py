from django.conf import settings
from django.core.cache import cache
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from .models import Users

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

def get_users_with_cache():
    activeUsers = []
    if 'activeUsers' in cache:
        print("active users not exist")
        activeUsers = cache.get('activeUsers')
    else:
        activeUsers = list(Users.objects.all())
        cache.set('activeUsers', activeUsers, timeout=CACHE_TTL)
        print("active users exist")
    return activeUsers
