# Generated by Django 2.0.5 on 2019-01-21 13:04

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('userActivity', '0006_auto_20190121_1833'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Users_Activity',
            new_name='Users',
        ),
    ]
