from .models import Users
from django.forms import ModelForm
from django.db import models
from django import forms

STATUS_CHOICES=(
    ('0',''),
    ('1','I m on call'),
    ('2','I m on conference room'))

class UserForm(ModelForm):
    status = forms.ChoiceField(choices = STATUS_CHOICES)
    class Meta:
        model = Users
        fields = ['status']
