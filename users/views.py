from django.shortcuts import render
from .cacheService import get_activity_time_with_cache
from .forms import UserForm

def index(request):
    print("hello")
    get_activity_time_with_cache()
    form = UserForm()
    return render(request, 'users/index.html',{'form':form})
    
